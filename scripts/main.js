

"use strict"

const tabsList = document.querySelectorAll('.tabs li');
const tabsContentContainer = document.querySelectorAll('.tabs-content li');

// tabsList.forEach(el => {
//       el.addEventListener('click', (event => {
//        el.classList.add('active');
    
//        tabsContentContainer.forEach(elem => {
//         elem.style.display = 'none';
//         if (elem.dataset.tab === event.target.innerText) {
//             elem.style.display = 'block';
//         }
//               })

//         }))
//       el.addEventListener('mouseout', (event => {
//         el.classList.remove('active');
//       }))
// })

tabsList.forEach(el => {
      el.addEventListener('click', (event => {
            el.classList.remove('active');
            if (el === event.target) { el.classList.add('active'); }

            tabsContentContainer.forEach(elem => {
                  elem.style.display = 'none';
                  if (elem.dataset.tab === event.target.innerText) {
                        elem.style.display = 'block';
                  }
            })

      }))
      el.addEventListener('mouseout', (event => {
            el.classList.remove('active');
      }))
})
